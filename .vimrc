" Mouse & backspace
set mouse=a
set bs=2

" Auto reload vimrc file
autocmd! bufwritepost .vimrc source %

" highlighted search
set hlsearch

" Copy/Paste
set pastetoggle=<F2>
set clipboard=unnamed

set background=dark
"set columns=80
"set tw=79
set nowrap
set fo-=t
highlight ColorColumn ctermbg=red
syntax on
set number

" c-programming stuff
set cindent
set shiftwidth=4
set tabstop=4
"tabs will be spaces 
set expandtab


" auto pairs
inoremap { 	{}<Left>
inoremap {<CR>	{<CR>}<Esc>O
inoremap {}	{}

inoremap ( 	()<Left>
inoremap (<CR>	(<CR>)<Esc>O
inoremap ()	() 

inoremap [	[]<Left>
inoremap [<CR>	[<CR>]<Esc>O
inoremap []	[]


" tabbing 
vnoremap < <gv
vnoremap > >gv

set laststatus=2

"#setlocal spell spelllang=de_de,en_us
"#set nospell
